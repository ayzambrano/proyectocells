#imagen Base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copia de archivos primera es origen (build) a la carpeta del contenedor
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto a exponer
EXPOSE 3000
